import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DateHelper } from './date.helper';

@Injectable({
  providedIn: 'root'
})
export class FormHelper {

  constructor(
    private dateHelper: DateHelper
  ) { }

  public validateFormAndDisplayErrors(form: FormGroup) {
    Object.keys(form.controls).map((controlName) => {
      form.get(controlName).markAsTouched({onlySelf: true});
    });
  }

  public setFormValue(form: FormGroup, value: any) {
    if (value) {
      Object.keys(value).forEach(name => {
        if (form.controls[name]) {
          form.controls[name].setValue(value[name]);
        }
      });
    }
  }

  public setMultiFormValue(form: FormGroup, value: any, list: string) {
    for (let i = 0; i < value.length; i++) {
      const controls = form.controls[list]['controls'][i].controls;
      const val = value[i];
      Object.keys(controls).forEach((control) => {
        let val = value[i][control];
        controls[control].setValue(val);
      });
    }
  }

  public convertModelToFormData(model: any, form: FormData = null, namespace = ''): FormData {
    let formData = form || new FormData();
    for (let propertyName in model) {
      if (!model.hasOwnProperty(propertyName) || model[propertyName] == undefined) continue;

      let formKey = namespace ? `${namespace}[${propertyName}]` : propertyName;
      if (model[propertyName] instanceof Date) {        
        formData.append(formKey, this.dateHelper.removeTimezoneFromDate(model[propertyName]));
      }
      else if (model[propertyName] instanceof Array) {
        model[propertyName].forEach((element, index) => {
          if (typeof element != 'object')
            formData.append(`${formKey}[]`, element);
          else {
            const tempFormKey = `${formKey}[${index}]`;
            this.convertModelToFormData(element, formData, tempFormKey);
          }
        });
      }
      else if (typeof model[propertyName] === 'object' && !(model[propertyName] instanceof File)) {        
        this.convertModelToFormData(model[propertyName], formData, formKey);
      }
      else {        
        formData.append(formKey, model[propertyName].toString());
      }
    }

    return formData;
  }

  public setControlValue(form: FormGroup, control: string, value: any): void {
    form.controls[control].setValue(value);
  }

  public fieldToString(val): string {
    return val ? val.toString() : null;
  }

  public getControlValue(formGroup: FormGroup, control: string) {
    return formGroup.controls[control] && formGroup.controls[control].value;
  }
}
