import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../../core/services/auth.service';
import { NavigationHelper } from '../../../../shared/helpers/navigation.helper';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  isLoading: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private navHelper: NavigationHelper
  ) {
    this.buildResetPasswordForm();
  }

  ngOnInit() {
  }

  public send(): void {
    if (this.resetPasswordForm.invalid) {
      return;
    }

    this.isLoading = true;
    this.auth.resetPassword(this.resetPasswordForm.value)
    .subscribe(() => {
      this.handleRequest(false);
    },
    err => {
      this.handleRequest(true);
    })
  }

  private handleRequest(isError): void {
    !isError && this.navHelper.goToPage('login');
    this.isLoading = false;
  }

  private buildResetPasswordForm(): void {
    this.resetPasswordForm = this.formBuilder.group({
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }
}
