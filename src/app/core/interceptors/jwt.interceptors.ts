import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { tap, take, filter, finalize, switchMap, catchError } from 'rxjs/internal/operators';
import { AuthService } from '../services/auth.service';
import { NavigationHelper } from '../../shared/helpers/navigation.helper';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  isRefreshingToken = false;

  private excludedURLs = [
    '/token',
  ];

  constructor(
    private auth: AuthService,
    private navHelper: NavigationHelper,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const tokenRequired = !this.excludedURLs.some(currURL => request.url.includes(currURL));
    if (tokenRequired) {
      const token = this.auth.getToken();
      if (!token) {
        this.auth.logOutCallback();
        return throwError('Token expired or missing');
      }
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + `${token}`
        }
      });
    }
    return next.handle(request)
      .pipe(
      tap(event => { }, err => {
        if (err.status === 401) {
          this.handle403Error(request, next)
            .subscribe((newToken) => {
              if (newToken && newToken.result) {
                this.auth.setTokenToLS(newToken.result);
                this.navHelper.goToPage('dashboard');
                this.tokenSubject.next(newToken.result.accessToken);
              }
              else {
                this.auth.logOutCallback();
              }
            },
            err => {
              this.auth.logOutCallback();
            });
        }
        return throwError(err);
      })
      )
  }

  handle403Error(req: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;
      this.tokenSubject.next(null);
      return this.auth.refreshToken()

    } else {
      return this.tokenSubject
        .pipe(
        filter(token => token != null),
        take(1),
        switchMap(accessToken => {
          req.headers.set('Authorization', accessToken);
          return next.handle(req);
        })
        )
    }
  }
}