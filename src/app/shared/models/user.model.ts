export interface UserModel {
    id: number;
    name: string;
    lastname: string;
  }