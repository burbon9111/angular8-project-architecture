export const environment = {
  production: true,
  HUB: 'http://production',
  URL: 'http://production/api',
  MEDIA: 'http://production/media'
};
