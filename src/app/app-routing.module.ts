import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: async () => {
        const { HomeModule } = await import('./modules/home/home.module');
        return HomeModule;
    },
    canLoad: [AuthGuard]
  },
  {
    path: 'sign-in',
    loadChildren: async () => {
        const { SignInModule } = await import('./modules/sign-in/sign-in.module');
        return SignInModule;
    }
  },
  {
    path: 'error',
    loadChildren: async () => {
        const { ErrorModule } = await import('./modules/error/error.module');
        return ErrorModule;
    }
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/error/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      scrollPositionRestoration: 'enabled'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
