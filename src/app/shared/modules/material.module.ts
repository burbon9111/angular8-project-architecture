import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    MatRadioModule,
    MatCheckboxModule,
    MatProgressSpinnerModule
} from '@angular/material';

@NgModule({
    declarations: [],
    imports: [
        MatButtonModule,
        MatSelectModule,
        MatInputModule,
        MatRadioModule,
        MatCheckboxModule,
        MatProgressSpinnerModule
    ],
    exports: [
        MatButtonModule,
        MatSelectModule,
        MatInputModule,
        MatRadioModule,
        MatCheckboxModule,
        MatProgressSpinnerModule
    ]
})
export class MaterialModule { }
