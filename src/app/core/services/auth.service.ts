import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Token } from 'src/app/shared/models/token.model';
import { UserModel } from 'src/app/shared/models/user.model';
import { HttpClient } from '@angular/common/http';
import { NavigationHelper } from 'src/app/shared/helpers/navigation.helper';

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  private apiPath = environment.URL;

  constructor(
    private http: HttpClient,
    private navHelper: NavigationHelper,
  ) {
    this.currentUserSubject = new BehaviorSubject<any>(this.getTokenFromLS());
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  public login(user: UserModel): Observable<UserModel> {
    return this.fetchToken(user)
      .pipe(map((userData) => {
        this.setTokenToLS({ ...userData.result });
        this.currentUserSubject.next({ ...userData.result });
        return userData;
      }));
  }

  public fetchToken(user: UserModel): Observable<any> {
    return this.http.post(`${this.apiPath}/Auth/token`, user);
  }

  public forgotPassword(email): Observable<any> {
    return this.http.post(`${this.apiPath}/Users/SentResetPassword`,
     { ...email, redirectUrl: `${environment.REDIRECT_URL}/login/reset-password` });
  }

  public resetPassword(confirmObj): Observable<any> {
    return this.http.post(`${this.apiPath}/Users/ConfirmResetPassword`, { ...confirmObj });
  }

  public isAuthenticated(): boolean {
    return this.currentUserSubject.value;
  }

  public getToken(): string | boolean {
    const token: Token = this.getTokenFromLS();
    return token && !helper.isTokenExpired(token.accessToken) && token.accessToken;
  }

  public refreshToken(): Observable<any> {
    let currentUser = this.getTokenFromLS();
    let token = currentUser.refreshToken;
    return this.http.post<any>(`${this.apiPath}/Auth/refresh_token`, { refreshToken: this.getRefreshToken() })
  }

  public logout(): Observable<any> {
    return this.http.post(`${this.apiPath}/auth/logout`, {});
  }

  public logOutCallback(): void {
    localStorage.removeItem('token');
    this.navHelper.goToPage('login');
    this.currentUserSubject.next(null);
  }

  public setTokenToLS(token: Token): void {
    localStorage.setItem('token', JSON.stringify(token));
  }

  private getRefreshToken(): string | boolean {
    const token: Token = this.getTokenFromLS();
    return token && token.refreshToken;
  }

  private getTokenFromLS(): Token {
    return JSON.parse(localStorage.getItem('token'));
  }
}
