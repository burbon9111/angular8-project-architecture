import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavigationHelper } from '../../../../shared/helpers/navigation.helper';
import { AuthService } from '../../../../core/services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  isLoading: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private navHelper: NavigationHelper
  ) {
    this.buildForgotPasswordForm();
  }

  ngOnInit() {
  }

  public send(): void {
    if (this.forgotPasswordForm.invalid) {
      return;
    }

    this.isLoading = true;
    this.auth.resetPassword(this.forgotPasswordForm.value)
    .subscribe(() => {
      this.handleRequest(false);
    },
    err => {
      this.handleRequest(true);
    })
  }

  private handleRequest(isError): void {
    !isError && this.navHelper.goToPage('login');
    this.isLoading = false;
  }


  private buildForgotPasswordForm(): void {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', Validators.required]
    });
  }
}
