import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DateHelper {

  constructor() { }

  public removeTimezoneFromDate(date): string {
    return new Date(date).toDateString();
  }

  public getDayNumber(date): string {
    return moment(date).format("DD")
  }

  public getDayOfWeek(date): string{
    return moment(date).format("ddd");
  }

  public addDays(date, days: number) {
    return moment(date, "YYYY/MM/DD HH:mm:mm").add(days, 'days').format('YYYY/MM/DD HH:mm:mm');
  }
}
