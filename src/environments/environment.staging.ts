export const environment = {
  production: true,
  HUB: 'http://staging',
  URL: 'http://staging/api',
  MEDIA: 'http://staging/media'
};
