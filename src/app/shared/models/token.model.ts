export interface Token {
  accessToken: string;
  issueAt: string;
  expiresAt: string;
  refreshToken: string;
  role: number;
  id: string;
}
