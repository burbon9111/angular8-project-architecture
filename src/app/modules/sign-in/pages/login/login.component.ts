import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../../core/services/auth.service';
import { first } from 'rxjs/operators';
import { NavigationHelper } from '../../../../shared/helpers/navigation.helper';
import { Constants } from '../../../../shared/constants/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isLoading = false;
  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private navHelper: NavigationHelper
  ) {
    this.buildLoginForm();
  }

  ngOnInit() {
  }

  public login(): void {
    if (this.loginForm.invalid) {
      return;
    }

    this.isLoading = true;
    this.auth.login(this.loginForm.value)
      .pipe(first())
      .subscribe(
      data => {
        this.handleRequest(false);
      },
      err => {
        this.handleRequest(true);
      });
  }

  private handleRequest(isError): void {
    !isError && this.navHelper.goToPage('home');
    this.isLoading = false;
  }

  private buildLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

}
