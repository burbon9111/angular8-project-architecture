import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/internal/operators/delay';

@Injectable({
  providedIn: 'root'
})
export class NavigationHelper {

  constructor(private router: Router) { }

  public goToPage(route: string): void {
    this.router.navigate([`/${route}`]);
  }

  public goToPageWithDelay(link: string, duration = 3000) {
    let redirectSurscription: Observable<any> = of(true).pipe(delay(duration));
    redirectSurscription.subscribe(() => {
      this.goToPage(link);
    })
  }
}
